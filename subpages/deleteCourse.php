<?php

include_once("DatabaseRepository/ConnectionRepository.php");
include_once("DatabaseRepository/CourseRepository.php");


$courseID = $_POST['courseID'];

$course = new Course();
$course->setCourseID($courseID);

$connectionRepository = new connectionRepository();

$connection = new mysqli('localhost',$connectionRepository->getUsername(),$connectionRepository->getPassword(),$connectionRepository->getDbName());

$connectionRepository->isConnected($connection);

$courseRepository = new CourseRepository($connection);

$courseRepository->databaseDeleteRowByID($course);

if($courseRepository->rowsAffected()){
    echo "<h2 class='align-center'>Usuwanie  z bazy danych zakończone <span style='color:green'>pomyślnie</span>.</h2>";
}
else{
    echo "<h2 class='align-center'>Usuwanie z bazy danych zakończone <span style='color:red'>niepowodzeniem</span>.</h2>";
}
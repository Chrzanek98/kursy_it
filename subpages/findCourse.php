<?php

include_once("DatabaseRepository/ConnectionRepository.php");
include_once("DatabaseRepository/CourseRepository.php");

$keyword = $_POST['searchCourseName'];

$course = new Course();
$course->setCourseKeywords($keyword);

$connectionRepository = new connectionRepository();

$connection = new mysqli('localhost',$connectionRepository->getUsername(),$connectionRepository->getPassword(),$connectionRepository->getDbName());

$connectionRepository->isConnected($connection);

$queryRepository = new CourseRepository($connection);

if(!$queryRepository->checkKeyword($keyword)){
    echo "<h2 class='align-center'>Nie podałeś żadnego parametru!</h2>";
    $connection->close();
    exit();
}

$result = $queryRepository->databaseSearch($course);

if($result->num_rows == 0){
    echo "<h2 class='align-center'>Nie ma wyników dla danego zapytania :(</h2>";

}
else {
    while($rowName = mysqli_fetch_assoc($result)) {
        echo    "<div class='col-sm-4 col-lg-2'>";
        echo    "<div class='thumbnail'>";
        echo    " <img src='/static/images/".(file_exists("static/images/".$rowName['courseName'].".png") ? $rowName['courseName'] : 'default').".png' class='img-responsive'>";
        echo    "<div class='caption'>";
        echo    "<h2 class='align-center'>".$rowName['courseName']."</h2>";
        echo    "<h4>Czas trwania: ".$rowName['courseLength']." ".($rowName['courseLength'] > 1 ? "dni" : "dzień")."</h4>";
        echo    "<form action='/index.php?id=courseDetails' method='post' style='display:inline;'>";
        echo    "<input type='hidden' name='courseID' value='".$rowName['id']."'/>";
        echo    "<button type='submit' class='btn btn-primary btn-sm' role='button'>Szczegóły</button></form>";
        echo    "<form action='/index.php?id=deleteCourse' method='post' style='display:inline;padding-left:3px;float:right'>";
        echo    "<input type='hidden' name='courseID' value='".$rowName['id']."'/>";
        echo    "<button type='submit' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-trash'></span></button></form>";
        echo    "<form action='/index.php?id=modifyCourses' method='post' style='display:inline;float:right;'>";
        echo    "<input type='hidden' name='courseID' value='".$rowName['id']."'/>";
        echo    "<button type='submit' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-edit'></span></button></form></div></div></div>";

    }
}
$result->free();
$connection->close();
<?php
include_once("DatabaseRepository/CheckModifyValue.php");
include_once("DatabaseRepository/ConnectionRepository.php");
include_once("DatabaseRepository/CourseRepository.php");


$courseId = $_POST['courseID'];

$course = new Course();
$course->setCourseID($courseId);

$connectionRepository = new connectionRepository();

$connection = new mysqli('localhost',$connectionRepository->getUsername(),$connectionRepository->getPassword(),$connectionRepository->getDbName());

$connectionRepository->isConnected($connection);

$fieldNames = new CourseRepository($connection);

$result = $fieldNames->searchById($course);

$rowName = $result->fetch_assoc();

?>

<div class="container-fluid songs-container">

    <div class="row">
        <div class="col-sm-4 col-md-2">
            <div class="panel panel-default">
                <div class="panel-body">
                            <img src="static/images/<?php echo (file_exists("static/images/".$rowName['courseName'].".png") ? $rowName['courseName'] : 'default') ?>.png" class="img-responsive">
                    <h1 class="align-center"><?php echo $rowName['courseName'];?></h1>
                    <button class="btn btn-primary btn-sm image-center"><span class="glyphicon glyphicon-bookmark"> Dołącz</span> </button>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-10">
            <h1 class="align-center">Naucz się języka <?php echo $rowName['courseName'];?> już dziś!</h1><br>
            <h2 class="align-center">Dzięki naszej pomocy zostaniesz programistą w <?php echo $rowName['courseLength'];?> dni</h2><br>
            <h2 class="align-center">Cena kursu to jedyne <?php echo round($rowName['coursePrice'],2); ?> $</h2><br>
            <h1 class="align-center" style="font-weight: bold">Zapisz się już dziś</h1><br>
        </div>
    </div>
</div>
<?php

include_once("DatabaseRepository/CheckModifyValue.php");
include_once("DatabaseRepository/ConnectionRepository.php");
include_once("DatabaseRepository/CourseRepository.php");


    $courseId = $_POST['courseID'];

    $course = new Course();
    $course->setCourseID($courseId);

    $connectionRepository = new connectionRepository();

    $connection = new mysqli('localhost',$connectionRepository->getUsername(),$connectionRepository->getPassword(),$connectionRepository->getDbName());

    $connectionRepository->isConnected($connection);

    $fieldNames = new CourseRepository($connection);

    $result = $fieldNames->searchById($course);
    $rowName = $result->fetch_assoc();

?>
<form class="form-horizontal" method="post" action="/index.php?id=finalModify">
    <div class="form-group">
        <label for="inputName" class="col-md-4 control-label">Nazwa kursu:</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="inputName" name="courseName" value="<?php echo $rowName['courseName'] ?>" placeholder="Podaj nazwę...">
        </div>
    </div>

    <div class="form-group">
        <label for="inputPrice" class="col-md-4 control-label">Cena:</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="inputPrice" name="coursePrice" value="<?php echo round($rowName['coursePrice'],2); ?>" placeholder="Podaj cenę...">
        </div>
    </div>

    <div class="form-group">
        <label for="inputLength" class="col-md-4 control-label">Czas trwania:</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="inputLength" name="courseLength" value="<?php echo $rowName['courseLength']; ?>" placeholder="Czas trwania...">
        </div>
    </div>

    <div class="form-group">
        <label for="inputKeywords" class="col-md-4 control-label">Słowa kluczowe:</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="inputKeywords" name="courseKeywords" value="<?php echo $rowName['courseKeywords']; ?>" placeholder="Słowa kluczowe (odzielane spacją)...">
        </div>
    </div>
    <input type="hidden" name="courseID" value="<?php echo $rowName['id'] ?>"
    <div class="form-group">
        <div class="form-actions">
            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Zatwierdź</button>
        </div>
    </div>
</form>

</body>
</html>
<?php

$connection->close();



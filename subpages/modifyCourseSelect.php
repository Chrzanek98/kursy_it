<?php
include_once('DatabaseRepository/HTMLFacade.php');
?>


<h2 class="align-center">Zaktualizuj jeden z kursów</h2>


<form class="form-horizontal" method="post" action="/index.php?id=modifyCourses">
    <div class="form-group">
        <div class="col-md-7 control-label">
            <label>Nazwa kursu:
                <select class="selectpicker" data-live-search="true" name="courseID">
                    <?php
                        $emptySelect = new HTMLFacade();
                        $emptySelect->makeOptions();
                    ?>
                </select>
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="form-actions">
            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> Modyfikuj</button>
        </div>
    </div>
</form>
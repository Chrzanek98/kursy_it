<?php

include_once("DatabaseRepository/ConnectionRepository.php");
include_once("DatabaseRepository/CourseRepository.php");
include_once("DatabaseRepository/PostDataRepository.php");

$courseID = $_POST['courseID'];
$courseName = $_POST['courseName'];
$coursePrice = $_POST['coursePrice'];
$courseLength = $_POST['courseLength'];
$courseKeywords = $_POST['courseKeywords'];

$course = new Course();
$course->setCourseID($courseID);
$course->setCourseName($courseName);
$course->setCoursePrice($coursePrice);
$course->setCourseLength($courseLength);
$course->setCourseKeywords($courseKeywords);

$connectionRepository = new connectionRepository();

$connection = new mysqli('localhost',$connectionRepository->getUsername(),$connectionRepository->getPassword(),$connectionRepository->getDbName());

$connectionRepository->isConnected($connection);

$checkData = new PostDataRepository($courseName,$coursePrice,$courseLength,$courseKeywords);


if(!$checkData->areSet()){
    echo "<h2 class='align-center'>Dodawanie kursu zakończone niepowodzeniem :(</h2> <h3 class='align-center'>Musisz uzupełnić <u>wszystkie</u> pola</h3>";
    $connection->close();
    exit();
}

if(!$checkData->isValid()){
    echo "<h1 class='align-center'>Formularz został wypełniony nieprawidłowo!</h1><br>
            <h2 class='align-center'><u>Zastosuj się do poniższych wymagań:</u></h2><br>
            <h3 class='align-center'>Nazwa kursu - litery i cyfry bez spacji</h3>
            <h3 class='align-center'>Cena - liczba całkowita lub zmiennoprzecikowa</h3>
            <h3 class='align-center'>Czas trwania - maksymalnie 3 cyfry</h3>
            <h3 class='align-center'>Słowa kluczowe - litery oraz cyfry, rodzielane spacją</h3>";
}
else {
    $courseRepository = new CourseRepository($connection);

    $courseRepository->databaseUpdateRow($course);

    if($courseRepository->rowsAffected()){
        echo "<h2 class='align-center'>Modyfikacja kursu  zakończona <span style='color:green'>pomyślnie</span>.</h2>";
    }
    else{
        echo "<h2 class='align-center'>Modyfikacja kursu zakończona <span style='color:red'>niepowodzeniem</span>.</h2>";
    }

}
$connection->close();


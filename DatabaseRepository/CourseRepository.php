<?php

require_once('Course.php');

class CourseRepository{
    private $_rowsAffected;
    private $_connection;


    public function __construct($connection){
        $this->_connection = $connection;
    }

    public function databaseReveal(){
        return $this->_connection->query("SELECT * FROM courses");
    }

    public function searchById(Course $course){
        $statement = $this->_connection->prepare("SELECT * FROM courses WHERE ID=?");
        $statement->bind_param("i",$course->getCourseID());
        $statement->execute();
        return $statement->get_result();
    }

    public function databaseSearch(Course $course){
        return $this->_connection->query("SELECT * FROM courses WHERE courseKeywords LIKE '%".$course->getCourseKeywords()."%'");
    }

    public function databaseAddRow(Course $course){
        $statement = $this->_connection->prepare("INSERT INTO courses (courseName,coursePrice,courseLength,courseKeywords) VALUES (?,?,?,?)");
        $statement->bind_param("sdis",$course->getCourseName(),$course->getCoursePirce(),$course->getCourseLength(),$course->getCourseKeywords());
        $statement->execute();
        $statement->close();
        $this->_rowsAffected = $this->_connection->affected_rows;
    }

    public function databaseUpdateRow(Course $course){
        $statement = $this->_connection->prepare("UPDATE courses SET courseName=?,coursePrice=?,courseLength=?,courseKeywords=? WHERE ID=?");
        $statement->bind_param("sdisi",$course->getCourseName(),$course->getCoursePirce(),$course->getCourseLength(),$course->getCourseKeywords(),$course->getCourseID());
        $statement->execute();
        $statement->close();
        $this->_rowsAffected = $this->_connection->affected_rows;

    }

    public function databaseDeleteRowByID(Course $course){
        $statement = $this->_connection->prepare("DELETE FROM courses WHERE id=?");
        $statement->bind_param("i",$course->getCourseID());
        $statement->execute();
        $statement->close();
        $this->_rowsAffected = $this->_connection->affected_rows;
    }

    public function rowsAffected(){
        return $this->_rowsAffected;
    }


    public function checkKeyword($keyword){
        $pureKeyword = trim($keyword);
        if(!$pureKeyword){
            return false;
        }
        else{
            return true;
        }
    }

}


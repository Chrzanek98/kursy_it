<?php

class PostDataRepository{
    private $courseName;
    private $coursePrice;
    private $courseLength;
    private $courseKeywords;

    public function __construct($courseName,$coursePrice,$courseLength,$courseKeywords){
        $this->courseName = $courseName;
        $this->coursePrice = $coursePrice;
        $this->courseLength = $courseLength;
        $this->courseKeywords = $courseKeywords;
    }

    public function areSet(){
        if(empty($this->courseName) || empty($this->coursePrice) || empty($this->courseLength) || empty($this->courseKeywords)){
            return false;
        }
        else{
            return true;
        }
    }

    public function isValid(){
        $nameRegex = '/^[\.\-]?[a-zA-Z+][a-zA-Z0-9#.]+$/';
        $priceRegex = '/[+]?([0-9]*[.])?[0-9]+/';
        $lengthRegex = '/^[0-9]{1,3}$/';
        $keywordsRegex = '/^[a-zA-Z0-9\s]*$/';

        if(preg_match($nameRegex,$this->courseName) && preg_match($priceRegex,$this->coursePrice)
            && preg_match($lengthRegex,$this->courseLength) && preg_match($keywordsRegex,$this->courseKeywords)){
            return true;
        }
        else{
            return false;
        }
    }
}
<?php


class CheckModifyValue{
    private $valueToChange;
    private $regex = null;

    public function __construct($valueToChange){
        $this->valueToChange = $valueToChange;
        $this->getValueRegex($this->valueToChange);
    }


    private function getValueRegex($value){
        if($value == 'courseName'){
            $this->regex = '/^[\.\-]?[a-zA-Z+][a-zA-Z0-9#.]+$/';
        }
        else if($value == "coursePrice"){
            $this->regex = '/[+]?([0-9]*[.])?[0-9]+/';
        }
        else if($value == "courseLength"){
            $this->regex = '/^[0-9]{1,3}$/';
        }
        else if($value == "courseKeywords"){
            $this->regex = '/^[a-zA-Z0-9\s]*$/';
        }

        return $this->regex;
    }
    public function validate($newValue){
        if(!is_null($this->regex)){
            if(preg_match($this->regex,$newValue)){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}
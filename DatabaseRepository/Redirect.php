<?php


class Redirect{
    private $id;

    public function __construct($id){
        $this->id = $id;
    }

    public function moveToSubpage(){
        if($this->id == "welcomeToCourse" || $this->id == null) {
            include('subpages/welcomeToCourse.html');
        }
        else if($this->id == "modifyCourseSelect") {
            include('subpages/modifyCourseSelect.php');
        }
        else if($this->id == "allCourses"){
            include("subpages/allCourses.php");
        }
        else if($this->id == "addCourse"){
            include("subpages/addCourse.php");
        }
        else if($this->id == "findCourse"){
            include("subpages/findCourse.php");
        }
        else if($this->id == "addCourseForm"){
            include("subpages/addCourse.html");
        }
        else if($this->id == "addCourse"){
            include("subpages/addCourse.php");
        }
        else if($this->id == "modifyCourses"){
            include("subpages/modifyCourses.php");
        }
        else if($this->id == "deleteCourse"){
            include("subpages/deleteCourse.php");
        }
        else if($this->id == "finalModify"){
            include("subpages/finalModify.php");
        }
        else if($this->id == "courseDetails"){
            include("subpages/courseDetails.php");
        }
    }
}
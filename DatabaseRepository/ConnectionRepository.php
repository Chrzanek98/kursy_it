<?php

class connectionRepository{
    private $userInfo;

    public function __construct(){
        $this->userInfo = parse_ini_file('config.ini');
    }

    public function getUsername(){
        return $this->userInfo['username'];
    }

    public function getPassword(){
        return $this->userInfo['password'];
    }

    public function getDbName(){
        return $this->userInfo['dbname'];
    }

    public function isConnected($connection){
        if($connection->connect_error){
            die("Connection failed: ".$connection->connect_error);
        }
    }
}

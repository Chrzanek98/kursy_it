<?php


class Course{

    private $_courseID;
    private $_courseName;
    private $_coursePrice;
    private $_courseLength;
    private $_courseKeywords;


    public function setCourseID($courseID){
        $this->_courseID = $courseID;
    }

    public function setCourseName($courseName){
        $this->_courseName = $courseName;
    }

    public function setCoursePrice($coursePrice){
        $this->_coursePrice = $coursePrice;
    }

    public function setCourseLength($courseLength){
        $this->_courseLength = $courseLength;
    }

    public function setCourseKeywords($courseKeywords){
        $this->_courseKeywords = $courseKeywords;
    }

    public function getCourseID(){
        return $this->_courseID;
    }

    public function getCourseName(){
        return $this->_courseName;
    }

    public function getCoursePirce(){
        return $this->_coursePrice;
    }

    public function getCourseLength(){
        return $this->_courseLength;
    }

    public function getCourseKeywords(){
        return $this->_courseKeywords;
    }

}
<?php
include_once('ConnectionRepository.php');
include_once('CourseRepository.php');


class HTMLFacade{

    private $connection;
    private $connectionRepository;
    private $result;

    public function __construct(){

        $this->connectionRepository = new connectionRepository();

        $this->connection = new mysqli('localhost',$this->connectionRepository->getUsername(),$this->connectionRepository->getPassword(),$this->connectionRepository->getDbName());

        $this->connectionRepository->isConnected($this->connection);

        $courseRepository = new CourseRepository($this->connection);

        $this->result = $courseRepository->databaseReveal();

    }

    public function makeOptions(){
        while($rowName = mysqli_fetch_assoc($this->result)) {
            echo "<option data-tokens='".$rowName['courseName']."' value='".$rowName['id']."'>".$rowName['courseName']."</option>";
        }
    }
}